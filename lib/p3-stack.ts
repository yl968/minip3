

import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
// import * as sqs from 'aws-cdk-lib/aws-sqs';
import * as lambda from 'aws-cdk-lib/aws-lambda';

export class P3Stack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);
    
    //create an S3 bucket with versioning and encryption enabled
    const bucket = new cdk.aws_s3.Bucket(this, 'XXXXXXXX', {
      versioned: true,
      encryption: cdk.aws_s3.BucketEncryption.S3_MANAGED,
    });

  }
}
